//
//  IRepository.swift
//  runapp
//
//  Created by user196354 on 6/18/21.
//

import Foundation

protocol IRunRepository {
    
    associatedtype T
    
    func find(byUUID uuid:String, completionHandler: (Result<T, Error>) -> Void)
    func findAll(completionHandler: (Result<[T], Error>) -> Void)
    func create(object: T, completionHandler: (Result<Void, Error>) -> Void)
    func update(object: T, completionHandler: (Result<Void, Error>) -> Void)
    func delete(id: Int, completionHandler: (Result<Void, Error>) -> Void)
    
}
