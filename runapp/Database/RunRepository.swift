//
//  RunRepository.swift
//  runapp
//
//  Created by user196354 on 6/19/21.
//

import Foundation
import CoreData

enum LocalDBError: Error {
    case badID
}

/// Implementation of the run repository
///
/// The database and model are created with the CoreData framework.

struct RunRepository: IRunRepository {
    
    // Currently available through constructor injection,
    // maybe property injection would be more appropriate
    private let context: NSManagedObjectContext
    
    init(_ context: NSManagedObjectContext) {
        self.context = context
    }
    
    typealias T = DomainRun

    func find(byUUID uuid: String, completionHandler: (Result<DomainRun, Error>) -> Void) {
        do {
            guard let objectIDUrl = URL(string: uuid) else {
                print("uuidstring is not a valid url")
                return
            }
            
            let coordinator = context.persistentStoreCoordinator
            
            // Coordinator should not be a nil
            guard let objectID = coordinator?.managedObjectID(forURIRepresentation: objectIDUrl) else {
                print("Coordinator was probably nil.")
                return
            }
            
            let object = try context.existingObject(with: objectID) as! Run
            
            // TODO: - Make it an extension
            let resultAsDomain = DomainRun(uuid: object.objectID.uriRepresentation().absoluteString, date: object.date!, distance: object.distance)
            
            completionHandler(.success(resultAsDomain))

            
        } catch {
            print("Error finding entity from RunRepository with error: \(error)")
        }
    }
    
    func findAll(completionHandler: (Result<[DomainRun], Error>) -> Void) {
        let request = NSFetchRequest<NSManagedObject>(entityName: "Run")
        
        do {
            let result = try context.fetch(request) as! [Run]
            var listDomainRun = [DomainRun]()
            for run in result {
                listDomainRun.append(DomainRun(uuid: run.objectID.uriRepresentation().absoluteString, date: run.date!, distance: run.distance))
            }
            
            completionHandler(.success(listDomainRun))
        } catch {
            print("Error finding all entities from RunRepository with error: \(error)")
        }
    }
    
    func create(object: DomainRun, completionHandler: (Result<Void, Error>) -> Void) {
        let entity = NSEntityDescription.entity(forEntityName: "Run", in: context)!
        let run = NSManagedObject(entity: entity, insertInto: context)
                
        run.setValue(object.date, forKey: "date")
        run.setValue(object.distance, forKey: "distance")
        
        do {
            try context.save()
            completionHandler(.success(()))
        } catch {
            print("Error creating entity in RunRepository with error: \(error)")
        }
    }
    
    func update(object: DomainRun, completionHandler: (Result<Void, Error>) -> Void) {
        
    }
    
    func delete(id: Int, completionHandler: (Result<Void, Error>) -> Void) {
        
    }
    
    
    

    

    
    func find(id: Int, completionHandler: (Run?, Error?) -> Void) {

    }
}
